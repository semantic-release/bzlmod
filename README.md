# `semantic-release/bzlmod`

> A `semantic-release` plugin to update the Bazel module version specified in `MODULE.bazel`.

| Step | Description |
| ---- | ----------- |
| verifyConditions | Verify that `MODULE.bazel` exists, `module.version` is specified in it and is writable. |
| prepare | Updates the version specified by `module.version` following semantic versioning. |

## Getting Started

```sh
npm config --location project set registry https://gitlab.arm.com/api/v4/groups/semantic-release/-/packages/npm/
npm install --save-dev @semantic-release/bzlmod
```

Add the following to `.releaserc.yaml`:

```yaml
plugins:
  - "@semantic-release/bzlmod"
```

## Usage

### Version Bumps

The plugin will update the `MODULE.bazel` with the next release version.

This is required by [bzlmod] to make sure that the `version` keyword in the `module()` definitions mirrors the actual project version.

To configure this scenario, combine the plugin with the `@semantic-release/git` plugin to add the version update to the release branch:

```yaml
plugins:
  - "@semantic-release/bzlmod"
  - path: "@semantic-release/git"
    assets:
      - MODULE.bazel
```

### `source.json` and archive generation

`bzlmod` expects to download source archives from a central registry.

It does this by looking for a `modules/${name}/${version}/source.json` at the provided `bazel --registry` argument(s).

However, this leads to an unfortunate release cycle:

- Perform release of project with `semantic-release/bzlmod`
- Create a merge request for an `bzlmod` registry such as the [BCR][bcr]
- Once merged, use the release

This can create a huge amount of churn, especially when performing pre-release `alpha`/`beta` releases.

The plugin has a secondary mode when it can generate an archive and `source.json` for the project.

The archive and `source.json` can be uploaded as release assets with `@semantic-release/gitlab` (or similar).

To configure this scenario, the plugin will be used a _second_ time (after the version bump):

```yaml
plugins:
  - "@semantic-release/bzlmod"
  - path: "@semantic-release/git"
    assets:
      - MODULE.bazel
  - path: "@semantic-release/bzlmod"
    source:
      prefix: ${CI_PROJECT_NAME}-v${version}
      url: ${CI_PROJECT_URL}/-/releases/v${version}/downloads/src.tar.gz
  - path: "@semantic-release/gitlab"
    assets:
      - path: MODULE.bazel
        type: package
        filepath: /modules/${env.CI_PROJECT_NAME}/${nextRelease.version}/MODULE.bazel
      - path: source.json
        type: package
        filepath: /modules/${env.CI_PROJECT_NAME}/${nextRelease.version}/source.json
      - path: src.tar.gz
        type: package
        filepath: /src.tar.gz
      - path: metadata.json
        type: package
        filepath: /modules/${env.CI_PROJECT_NAME}/metadata.json
```

`bzlmod` can then be pointed directly at the GitLab release to use the project. This negates the need to upload all released versions to a `bzlmod` registry.

Set the `--registry` to the release URL (`${CI_PROJECT_URL}/-/releases/v${version}`).

## Configuration

By default the plugin will locate `MODULE.bazel` in the current package root `.`

### `filepath`

The path to `MODULE.bazel` can be specified in the plugin configuration. 

For example, to specify `MODULE.bazel` in `./custom/`, do:

```yaml
plugins:
  - path: "@semantic-release/bzlmod"
    filepath:
      - "./custom/MODULE.bazel"
```

### `source`

The plugin can automatically create a `source.json` which can be used to integrate with `bzlmod`.

It *must* be performed _after_ the `@semantic-release/git` to generate the correct integrity for the source tarball.

The `prefix`, `archive` and `url` must be specified which can use environment variables.

Often the `source` keywords use the CI environment keywords to point the `source.json` back to a release within the version control system being used (GitLab, GitHub, etc).

[bzlmod]: https://bazel.build/build/bzlmod
[bcr]: https://github.com/bazelbuild/bazel-central-registry
