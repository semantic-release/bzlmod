import fs from 'node:fs';
import path from 'node:path';
import {readFile, writeFile, mkdir, chmod, unlink} from 'node:fs/promises';
import test from 'ava';
import {stub} from 'sinon';
import clearModule from 'clear-module';
import {WritableStreamBuffer} from 'stream-buffers';
import {temporaryDirectory} from 'tempy';
import git from 'isomorphic-git';
import which from 'which';
import ssri from 'ssri';
import {$} from 'execa';

test.beforeEach(async t => {
	clearModule('../plugin.mjs');
	t.context.m = await import('../plugin.mjs');
	t.context.log = stub();
	t.context.tmp = temporaryDirectory();
	const dir = temporaryDirectory();

	t.context.source = path.join(dir, 'source.json');
	t.context.archive = path.join(dir, 'archive.tar');
	t.context.metadata = path.join(dir, 'metadata.json');
	t.context.touch = path.join(dir, 'touch.txt');
	await writeFile(t.context.touch, 'Hello, world!');
	t.context.workspace = path.join(dir, 'WORKSPACE');
	await writeFile(t.context.workspace, '');

	t.context.filepath = path.join(dir, 'nested', 'MODULE.bazel');
	await mkdir(path.dirname(t.context.filepath));
	await writeFile(t.context.filepath, 'module(version = "0.0.0")\nbazel_dep(name = "foo")');

	await git.init({fs, dir});
	t.context.date = new Date(2022, 5, 6, 6, 6, 6);
	t.context.signature = {
		name: 'Bave Drown',
		email: 'bave.drown@epic.kek',
		timestamp: Math.floor(t.context.date / 1000),
		timezoneOffset: t.context.date.getTimezoneOffset(),
	};
	await git.add({fs, dir, filepath: path.relative(dir, t.context.touch)});
	await git.add({fs, dir, filepath: path.relative(dir, t.context.filepath)});
	await git.commit({fs, dir, author: t.context.signature, committer: t.context.signature, message: 'initial'});

	t.context.cfg = {
		filepath: path.relative(dir, t.context.filepath),
		source: {
			url: '${HOST}/there/${nextRelease.version}', // eslint-disable-line no-template-curly-in-string
			prefix: '${PROJECT}-${nextRelease.version}', // eslint-disable-line no-template-curly-in-string
			output: path.relative(dir, t.context.source),
			archive: path.relative(dir, t.context.archive),
			metadata: path.relative(dir, t.context.metadata),
		},
	};
	t.context.ctx = {
		cwd: dir,
		env: {
			HOST: 'https://some.com',
			PROJECT: 'whatever',
		},
		options: {},
		stdout: new WritableStreamBuffer(),
		stderr: new WritableStreamBuffer(),
	};
	t.context.ctx.logger = {
		log: t.context.log,
		info: t.context.log,
		success: t.context.log,
		warn: t.context.log,
	};
});

const failure = test.macro(
	async (t, before, after) => {
		if (before) {
			await before(t);
		}

		const [, code, pattern] = /`(.+)` error when (.+)/.exec(t.title);
		const regex = new RegExp(`${pattern[0].toUpperCase()}${pattern.slice(1)}`);

		const error = await t.throwsAsync(t.context.m.verifyConditions(t.context.cfg, t.context.ctx));

		t.is(error.name, 'SemanticReleaseError', `${error}`);
		t.is(error.code, code, `${error}`);
		t.regex(error.message, regex, `${error}`);

		if (after) {
			await after(t);
		}
	},
);

const success = test.macro(
	async (t, before, after) => {
		if (before) {
			await before(t);
		}

		await t.notThrowsAsync(t.context.m.verifyConditions(t.context.cfg, t.context.ctx));
		t.context.ctx.nextRelease = t.context.ctx.nextRelease || {version: '1.0.0', type: 'major'};
		await t.notThrowsAsync(t.context.m.prepare(t.context.cfg, t.context.ctx));
		await t.notThrowsAsync(t.context.m.verifyConditions(t.context.cfg, t.context.ctx));

		const {cwd, nextRelease: {version: expected}} = t.context.ctx;
		const {filepath} = t.context.cfg;
		const {stdout: version} = await $`buildozer -root_dir ${cwd} ${'print version'} //${filepath}:%module`;
		const {stdout: level} = await $`buildozer -root_dir ${cwd} ${'print compatibility_level'} //${filepath}:%module`;
		t.is(version, expected);
		const l = `${t.context.level || expected.split('.')[0]}`;
		t.is(level, l);

		if (after) {
			await after(t);
		}
	},
);

test('Throws `EBZLMODREAD` error when failed to read `.+`', failure, async t => {
	await unlink(t.context.filepath);
});

test('Throws `EBZLMODMISSINGMODULE` error when `.+` `module` does not exist', failure, async t => {
	await writeFile(t.context.filepath, 'bazel_dep(name ="whatever", version = "0.0.0")');
});

test('Throws `EBZLMODWRITE` error when failed to write `.+`', failure, async t => {
	chmod(t.context.filepath, '444');
});

test('Throws `EBZLMODCFG` error when `source` must be a mapping', failure, async t => {
	t.context.cfg.source = [];
});

test('Throws `EBZLMODCFG` error when `filepath` must be a string', failure, async t => {
	t.context.cfg.filepath = 1;
});

test('Throws `EBZLMODCFG` error when `source.prefix` must be a string', failure, async t => {
	delete t.context.cfg.source.prefix;
});

test('Throws `EBZLMODCFG` error when `source.archive` must be a string', failure, async t => {
	t.context.cfg.source.archive = 1;
});

test('Throws `EBZLMODCFG` error when `source.metadata` must be a string', failure, async t => {
	t.context.cfg.source.metadata = 1;
});

test('Throws `EBZLMODCFG` error when `source.output` must be a string', failure, async t => {
	t.context.cfg.source.output = 1;
});

test('Throws `EBZLMODCFG` error when `source.output` failed to be templated', failure, async t => {
	t.context.cfg.source.output += '${NOPE}'; // eslint-disable-line no-template-curly-in-string
});

test('Throws `EBZLMODCFG` error when `source.url` must be a string', failure, async t => {
	delete t.context.cfg.source.url;
});

test('Throws `EBZLMODCFG` error when `source.url` must be a valid URL', failure, async t => {
	t.context.cfg.source.url = '~:"!';
});

test('Throws `EBZLMODCFG` error when `source.url` failed to be templated', failure, async t => {
	delete t.context.ctx.env.HOST;
});

test('Throws `EBZLMODCFG` error when `source.archive` failed to be templated', failure, async t => {
	t.context.cfg.source.archive += '${NOPE}'; // eslint-disable-line no-template-curly-in-string
});

test('Throws `EBZLMODCFG` error when `source.archive` has unsupported extension', failure, async t => {
	t.context.cfg.source.archive += '.abc';
});

test('Throws `EBZLMODCFG` error when `source.metadata` failed to be templated', failure, async t => {
	t.context.cfg.source.metadata += '${NOPE}'; // eslint-disable-line no-template-curly-in-string
});

test('Throws `EBZLMODCFG` error when `source.prefix` failed to be templated', failure, async t => {
	delete t.context.ctx.env.PROJECT;
});

test('Throws `EBZLMODCFG` error when failed to find `.+` CLI', failure, async t => {
	t.context.cfg.git = 'nope/not/here';
});

test('Throws `EBZLMODCFG` error when failed to run `.+` CLI', failure, async t => {
	const {pathname} = new URL(import.meta.url);
	t.context.cfg.git = path.join(path.dirname(pathname), 'fixture', 'fail.mjs');
});

for (const extension of ['.tar', '.zip', '.tar.gz']) {
	(which.sync('git') ? test : test.skip)(`Verify and prepare the package${extension}`, success, async t => {
		t.context.cfg.source.archive = t.context.cfg.source.archive.replace('.tar', extension);
		t.context.archive = t.context.archive.replace('.tar', extension);
	}, async t => {
		const source = JSON.parse(await readFile(t.context.source));
		t.is(typeof source.url, 'string');
		t.is(typeof source.integrity, 'string');
		t.is(typeof source.strip_prefix, 'string');
		const archive = fs.createReadStream(t.context.archive);
		const integrity = await ssri.fromStream(archive);
		t.is(integrity.toString(), source.integrity);
		const metadata = JSON.parse(await readFile(t.context.metadata));
		const {nextRelease: {version}} = t.context.ctx;
		t.deepEqual(metadata.versions, [version]);
	});
}

test('Verify and prepare the package when no `source`', success, async t => {
	delete t.context.cfg.source;
});

test('Verify and prepare the package when no `source.output`', success, async t => {
	delete t.context.cfg.source.output;
});

test('Verify and prepare the package when no `source.archive`', success, async t => {
	delete t.context.cfg.source.archive;
});

test('Verify and prepare the package when no `source.metadata`', success, async t => {
	delete t.context.cfg.source.metadata;
});

test('Verify and prepare the package when `compatibilty_level` is set', success, async t => {
	delete t.context.cfg.source;
	await writeFile(t.context.filepath, 'module(version = "0.0.0", compatibility_level = 3)');
	t.context.level = 4;
});

test('Verify and prepare the package when `compatibilty_level` should not be bumped', success, async t => {
	delete t.context.cfg.source;
	await writeFile(t.context.filepath, 'module(version = "0.0.0", compatibility_level = 3)');
	t.context.ctx.currentRelease = {version: '1.0.0'};
	t.context.ctx.nextRelease = {version: '1.1.0'};
	t.context.level = 3;
});
