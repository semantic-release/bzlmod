// eslint-disable-next-line unicorn/prefer-module
const test = require('ava');

test('Can load the CommonJS module', async t => {
	// eslint-disable-next-line unicorn/prefer-module
	const {verifyConditions, prepare} = require('../plugin.js');
	const {temporaryDirectory} = await import('tempy');
	t.context.cfg = {filepath: 'MODULE.bazel'};
	t.context.ctx = {
		cwd: temporaryDirectory(),
	};

	t.is(typeof verifyConditions, 'function');
	t.is(typeof prepare, 'function');
	await t.throwsAsync(
		verifyConditions(t.context.cfg, t.context.ctx),
	);
	await t.throwsAsync(
		prepare(t.context.cfg, t.context.ctx),
	);
});

