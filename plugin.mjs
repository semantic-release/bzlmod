import {readFile, writeFile} from 'node:fs/promises';
import {pipeline} from 'node:stream/promises';
import {PassThrough} from 'node:stream';
import {createGzip} from 'node:zlib';
import {createWriteStream} from 'node:fs';
import path from 'node:path';
import SemanticReleaseError from '@semantic-release/error';
import debug from 'debug';
import {template} from 'lodash-es';
import ssri from 'ssri';
import {execa, $} from 'execa';

debug('semantic-release:bzlmod');

function extension(path) {
	const [tar, ext] = path.split('.').slice(-2);

	if (tar === 'tar') {
		return `.${tar}.${ext}`;
	}

	return `.${ext}`;
}

const COMPRESSION = {
	'.tar': () => new PassThrough(),
	'.tar.gz': () => createGzip({level: 9}),
	'.zip': () => new PassThrough(),
};

export async function verifyConditions(pluginConfig, context) {
	const {git = 'git', filepath = 'MODULE.bazel'} = pluginConfig;
	const {cwd, logger, env} = context;

	if (typeof filepath !== 'string') {
		throw new SemanticReleaseError('`filepath` must be a string', 'EBZLMODCFG', `${filepath} (${typeof filepath})`);
	}

	const bzlmod = path.join(cwd, filepath);

	const data = await (async () => {
		try {
			debug('Reading `%s`', bzlmod);
			return await readFile(bzlmod, {encoding: 'utf8'});
		} catch (error) {
			throw new SemanticReleaseError(`Failed to read \`${bzlmod}\``, 'EBZLMODREAD', `${error}`);
		}
	})();

	debug('Finding `module` in `%s`', bzlmod);
	const {stdout} = await $`buildozer -root_dir ${cwd} ${'print kind'} //${filepath}:%module`;
	if (stdout !== 'module') {
		throw new SemanticReleaseError(
			`\`${bzlmod}\` \`module\` does not exist`,
			'EBZLMODMISSINGMODULE',
		);
	}

	try {
		debug('Writing `%s`', bzlmod);
		await writeFile(bzlmod, data);
	} catch (error) {
		throw new SemanticReleaseError(`Failed to write \`${bzlmod}\``, 'EBZLMODWRITE', `${error}`);
	}

	logger.success('Validated `%s`', bzlmod);

	const {source} = pluginConfig;

	if (source === undefined) {
		logger.warn('No `source` configuration, will not generate `source.json`');
		return;
	}

	if (typeof source !== 'object' || Array.isArray(source)) {
		throw new SemanticReleaseError('`source` must be a mapping', 'EBZLMODCFG', `${source} (${typeof source})`);
	}

	let {url, prefix, archive = 'src.tar', output = 'source.json', metadata = 'metadata.json'} = source;
	if (typeof url !== 'string') {
		throw new SemanticReleaseError('`source.url` must be a string', 'EBZLMODCFG', `${url} (${typeof url})`);
	} else if (typeof prefix !== 'string') {
		throw new SemanticReleaseError('`source.prefix` must be a string', 'EBZLMODCFG', `${prefix} (${typeof prefix})`);
	} else if (typeof archive !== 'string') {
		throw new SemanticReleaseError('`source.archive` must be a string', 'EBZLMODCFG', `${archive} (${typeof archive})`);
	} else if (typeof metadata !== 'string') {
		throw new SemanticReleaseError('`source.metadata` must be a string', 'EBZLMODCFG', `${metadata} (${typeof metadata})`);
	} else if (typeof output !== 'string') {
		throw new SemanticReleaseError('`source.output` must be a string', 'EBZLMODCFG', `${output} (${typeof output})`);
	}

	const version = '0.0.0';
	const ctx = {...env, version, env, nextRelease: {version}};

	try {
		url = template(url)(ctx);
	} catch (error) {
		throw new SemanticReleaseError('`source.url` failed to be templated', 'EBZLMODCFG', `${error}`);
	}

	try {
		prefix = template(prefix)(ctx);
	} catch (error) {
		throw new SemanticReleaseError('`source.prefix` failed to be templated', 'EBZLMODCFG', `${error}`);
	}

	try {
		archive = template(archive)(ctx);
	} catch (error) {
		throw new SemanticReleaseError('`source.archive` failed to be templated', 'EBZLMODCFG', `${error}`);
	}

	if (!Object.keys(COMPRESSION).includes(extension(archive))) {
		throw new SemanticReleaseError('`source.archive` has unsupported extension', 'EBZLMODCFG', `${extension(archive)} not in ${Object.keys(COMPRESSION).join(', ')}`);
	}

	try {
		metadata = template(metadata)(ctx);
	} catch (error) {
		throw new SemanticReleaseError('`source.metadata` failed to be templated', 'EBZLMODCFG', `${error}`);
	}

	try {
		output = template(output)(ctx);
	} catch (error) {
		throw new SemanticReleaseError('`source.output` failed to be templated', 'EBZLMODCFG', `${error}`);
	}

	try {
		url = new URL(url);
	} catch (error) {
		throw new SemanticReleaseError('`source.url` must be a valid URL', 'EBZLMODCFG', `${error}`);
	}

	try {
		await execa(git, ['--version']);
	} catch (error) {
		if (error.code === 'ENOENT') {
			throw new SemanticReleaseError(`Failed to find \`${git}\` CLI`, 'EBZLMODCFG');
		}

		throw new SemanticReleaseError(`Failed to run \`${git}\` CLI`, 'EBZLMODCFG', `${error}`);
	}

	logger.success('Validated `source` configuration');
	logger.info('`source.prefix` is `%s`', prefix);
	logger.info('`source.url` is `%s`', url);
	logger.info('`source.output` is `%s`', output);
	logger.info('`source.archive` is `%s`', archive);
	logger.info('`source.metadata` is `%s`', metadata);
}

export async function prepare(pluginConfig, context) {
	const {git = 'git', filepath = 'MODULE.bazel'} = pluginConfig;
	const {
		cwd,
		env,
		logger,
		nextRelease: {version, type: kind},
	} = context;

	const {stdout: v} = await $`buildozer -root_dir ${cwd} ${'print version'} //${filepath}:%module`;
	if (v !== version) {
		logger.log('Replacing version `%s` with `%s`', v, version);
		const set = `set version ${version}`;
		await $`buildozer -root_dir ${cwd} ${set} //${filepath}:%module`;
	}

	const {stdout: o} = await $`buildozer -root_dir ${cwd} ${'print compatibility_level'} //${filepath}:%module`;
	const l = Number.parseInt(o, 10);
	const [major] = version.split('.');
	const level = Number.isNaN(l) ? Number.parseInt(major, 10) : (kind === 'major' ? l + 1 : l);
	if (l !== level) {
		logger.log('Replacing level `%s` with `%s`', l, level);
		const set = `set compatibility_level ${level}`;
		await $`buildozer -root_dir ${cwd} ${set} //${filepath}:%module`;
	}

	const {source} = pluginConfig;

	if (source === undefined) {
		return;
	}

	let {
		url,
		prefix,
		output = 'source.json',
		archive = 'src.tar',
		metadata = 'metadata.json',
	} = source;
	const ctx = {...env, version, env, nextRelease: {version}};

	prefix = path.normalize(template(prefix)(ctx));
	url = new URL(template(url)(ctx));
	output = path.join(cwd, template(output)(ctx));
	archive = path.join(cwd, template(archive)(ctx));
	metadata = path.join(cwd, template(metadata)(ctx));

	const ext = extension(archive);
	const format = ext.startsWith('.tar') ? 'tar' : 'zip';
	const process = execa(git, ['-C', cwd, 'archive', `--format=${format}`, `--prefix=${prefix}/`, 'HEAD']);
	const hasher = ssri.integrityStream();
	const promise = new Promise((resolve, reject) => {
		hasher.on('integrity', resolve);
		hasher.on('error', reject);
	});
	const compression = COMPRESSION[extension(archive)]();
	await pipeline(process.stdout, compression, hasher, createWriteStream(archive));
	const integrity = await promise;
	logger.success('Wrote `%s` (`%s`)', archive, integrity);

	const src = {
		url: `${url}`,
		integrity: `${integrity}`,
		strip_prefix: prefix, // eslint-disable-line camelcase
	};
	await writeFile(output, JSON.stringify(src));
	logger.success('Wrote `%s`', output);
	logger.info('`%j`', src);

	const meta = {
		versions: [version],
	};
	await writeFile(metadata, JSON.stringify(meta));
	logger.success('Wrote `%s`', output);
	logger.info('`%j`', meta);
}
